\section*{Introduction}
In this evaluation document we assess the performance of the data structures and compare them to our hypotheses formulated in the design-document. All the results we obtain are explained and supported with numbers and some of them with charts. 

% Notes
Notes: 
\begin{itemize}
	\item We have renamed the \textit{data\_structures} namespace into \textit{pcm} (as from '\textbf{P}roject for \textbf{C}oncurrency and \textbf{M}ultithreading') because we used a private GIT repository where all our other projects have names related with their correspondent courses (hope is not a problem). 
	\item Also \textit{implementation} package is split in two - \textit{lists} and \textit{trees} for a better separation of code.
	\item Besides the packages for the code of the data structures, we have included a \textit{test} package used for debugging, validation and performance tests.
	\item We modified the Main driver to also receive as an argument the boolean flag for duplicates (we need it to automatize our tests).
\end{itemize}

% Java Implementation - CHAPTER_BEGIN
\section{A Short Review on the Data Structures' Implementation in Java}

\subsection{Additions to the Design Document's Specs \cite{tag_designdoc}}
In this subsection we present a couple of interesting aspects, as additions to the design document. Some of them we considered simpler than we thought (but we were convinced by your email-replies that are more than that), for others we lacked knowledge (ABA problem was discussed a week ago) and others drew our attention at the moment we have began to write the Java code. 

\subsubsection*{Representation of  +/- Infinity}
Our idea of representing \textit{-Infinity} and \textit{+Infinity} nodes (the sentinels used in the book and article to prevent particular cases) is based on the compareTo() method that we defined in our Types' Definition (i.e. see ListNode base class from List Types' Definition \cite{tag_designdoc}, that implements Comparable<ListNode<T>>). Because is a higher level of compareTo() - node level, node.compareTo(other\_node), and not item level - we expressed the \textit{-Infinity} and \textit{+Infinity} using two boolean flags InfinityPlus and InfinityMinus. Flags are set after a node's creation and used in the compareTo() method to verify the Infinity cases. If none of the flags is actually set then we use the compareTo() from the type <T> implementation, item.compareTo(), otherwise we return -1 or +1 depending on what Infinity case we have. So, our program-logic does not change at all. We did not add extra logic such as \textit{if (obj instanceof Cls)} statements. We simply wrapped our \textit{item} into a temporary node which we used for later comparisons. As an exception, CoarseGrainedTree and FineGrainedTree do not use this idea. For these two data structures we basically made a root null value node and considered it \textit{+Infinity} by default (we always traverse to the left when we start from the root, without making a compareTo). 
\newline \newline
Example of the compareTo() method implemented in the ListNode<T> base class:
\begin{lstlisting}
public int compareTo(ListNode<T> node) {
	if (_item == null && node.getItem() == null) {
		return 0; 
	}
		
	if (_item == null) {
		return ((_minusInfinity) ? (-1) : (1));
	}
		
	if (node.getItem() == null) {
		return ((node.isMinusInfinity()) ? (1) : (-1));
	}
						
	return _item.compareTo(node.getItem());
}
\end{lstlisting}

\subsubsection*{ABA Problem - LockFreeTree}
In the LockFreeTree article \cite{tag_article} the authors refer to CompareAndSwap() calls. This is an abstract operation that in Java does not exists as it is, but may be implemented as a CompareAndSet() call on a AtomicReference object (as we did). We claim that for this data structure we can follow the article's algorithm implementation without major modifications to the RMW operations. Therefore is no need of AtomicStampedReference to solve ABA problem since:
\begin{itemize}
	\item We apply CompareAndSet using (as call arguments) references to dynamic allocated memory, not trivial data types, in a Automatic Garbage Collection that deals with this problem by ensuring that threads are using unique references to the memory. Memory recycling is managed by the Garbage Collector in such way we cannot use the same reference if other thread can reach to it through a chain of other references.
	\item As mentioned in \cite{tag_article}, << each time a node is flagged, its \textit{info} field contains a pointer to a new info record, so it will always be different from any value previously stored there. When the state field is subsequently changed back to \textit{Clean} by a unflag or backtrack CAS, we leave the pointer to the info record in the \textit{info} field so that the CAS object (\textit{update} object) that contains these two fields has a value that is different from anything that was previously stored there. This scheme should not introduce any problems as long as a memory location is not reallocated while any process could reach that location by following a chain of pointers. Such a safe garbage collection schemes are often provided, for example, by the Java environment. If this causes complications for automatic garbage collection because of cycles of pointers between tree nodes and Info records, clean update fields could instead have a counter attached to them \textit{to serve the same purpose}. >> This strengthens our first claim.
\end{itemize}

\subsubsection*{Completion to the LockFreeList Algorithm}
When we began to write the Java code we observed another modification that had to be made on the code provided in the book (for the LockFreeList) in order to deal with duplicate numbers. Suppose if we do not allow duplicates, what happens if two threads want to remove the same element - remove(X)? Well, in worst case they overlap, but the threads will eventually remove X, one will do the job and the other will fail on CAS and return successfully. Since we do not allow duplicates, is correct to return without trying to remove again our element - just one X could exist at that moment and that one was removed by the helper thread. If we allow duplicate numbers, the logic changes a little bit. Now we have to retry our remove operation in case we fail at CAS. Why? Well, suppose that find() returns the first element equal with \textit{item} and since the list has duplicates then is possible that find() returns the same position in the list for two threads that want to remove the same element. This is different than the help case, because now a remove failure does not truly mean that another thread is helping us - it could mean that the thread is removing it's own element. How did we observed this flaw? We tested the data structure on examples with many duplicates (i.e. in Main we put a \% 50 in the number generation method) so that the chance of duplicate remove contention to be very high.

\subsubsection*{Mistake/False-claim on CoarseGrainedList Implementation Approach}
In the feedback you suggested that we made a false claim about the implementation approach regarding the CoarseGrainedList. We have looked over the design document and we, first of all, have found a sign mistake (typo):

..............

\textit{In this case we use greater or equal (compareTo(item)} \textbf{\_ >= \_} \textit{0) for insertion, and obviously equal first  (compareTo(item) == 0) for removal.}

..............

Also, the above mentioned predicates/expressions do not refer to the while-conditions. We just wanted to point out the conditions that the \textit{curr} element (the one that the while loop stops at) should pass in order to be the element on which we perform our add/remove. In a while statement we would have, for \textit{ add(): while(curr.compareTo(item) < 0) \{ //traverse \} }, and for \textit{remove()} the same. At the end of while, for the \textit{add} is not necessary to reverify that \textit{curr.compareTo(item) >= 0 } - is redundant because of the while condition -, but for the remove is necessary to check if equals \textit{item} - otherwise we can remove a non-equal element.

\textit{It is possible that this is not the claim you referred to. In the pseudocode, which we provided, we think that the algorithm's idea is probably explained better (this is one of the reasons we put the pseudocode in our design document).}

\subsection{Custom Build Test Driver}
Besides the Main class that you have provided, we tested our implementation performance also with a custom build driver. Instead of an equal work distribution + barrier inbetween he have chosen a more flexible test architecture based on the Work-Crew (work stealing) paradigm. Thus, we defined three abstract entities \textit{Work}, \textit{Worker} and \textit{WorkPool}, a synchronized linked list. A \textit{Work} is represented by an operation performed on a \textit{Sorted} data structure - \textit{Add/RAdd/Remove}. A \textit{RAdd} means a removable add, an add that has a remove as a pair; when a thread sees that the performed add(X) - \textit{X was added} - operation was removable, it inserts in the \textit{WorkPool} an extra \textit{Work} for removing X from the list, remove(X). The \textit{RAdds} guarantees that every add has a remove as a pair and in the end that element no longer should exist in the list. We included the \textit{RAdds} since we cannot pair on-the-fly normal random-number \textit{Adds} with \textit{Removes}, unless we do a preprocessing of an items' list before run. Moreover, by having those three operations, we could create many test-cases. Since the \textit{Work} is scrambled when the \textit{WorkPool} is set we achieved overlapping between \textit{Adds} and \textit{Removes}. Also, we could simulate a work-stealing-barrierless behaviour of the Main class by performing only \textit{RAdds} operations.

Another thing to point out is that we split our tests in two: \textit{Validation Tests}, to check if given the same order of operations and elements (no duplicates, to apply the test also on LockFreeTree) all the data structures output the same results - this gives us a somehow practical prove that our implementation is correct, and \textit{Performance Tests} where we overlap operations and we 'play' with the call's arguments in order to validate our hypotheses formulated in the design doc.
% Java Implementation - CHAPTER_END

%% BREAK SECTION %%

% Evaluation - CHAPTER_BEGIN
\section{Evaluation}
For the implementation's evaluation we used both the Main and our custom build driver. Only by using the Main driver program is very hard to observe, on a commodity computer (i.e my dual core I7), the improvements of each data structure, especially the ones implemented at the lists. The Main driver definitely scales, but forces (N - 1) threads to wait at the barrier for the Nth thread to finish. Therefore, because we wanted to be sure that our hypotheses were close to the truth, we also used our custom build driver only with the \textit{RAdds} option available (to behave similar with the Main class). Though, we could have used our custom build driver to evaluate the performances on a mix of overlapping \textit{Adds/RAdds/Removes} we preferred adds followed by removes (as \textit{RAdds}) to emphasize the case when the list/tree keeps growing bigger and bigger (adds) and then decreases (removes). We let the mix of overlapping \textit{Adds/RAdds/Removes} (the daily/real use of a data structure) at the \textit{extra/extension section}, at the end of this document.
\newline \newline
% Notes
Platform testing configuration: 
\begin{itemize}
	\item \textbf{Cluster-Node}: 1 Node @ DAS4, 16 x QuadCore Intel Xeon CPU E5620 @ 2.4GHz 
	\item \textbf{Magny-Cours}:  48-core machine [\textit{extra - stress test}]
\end{itemize}

\subsection{Test Results}
Legend: \textit{Wt} is the workTime that a thread spends on doing an operation (microsec), \textit{Sz} is the set size (the number of elements added to the list/tree), \textit{Th} represents the number of threads used for test, \textit{F} means NO duplicates allowed and \textit{T} that duplicates are allowed, cgl/fgl/lfl/cgt/fgt/lft are the data structures' names abbreviations, and the time is an average over 5 runs, measured in milliseconds.

\pagebreak

\subsection*{Platform: Cluster-Node}

\subsubsection*{Test results based on Main driver}

Constant work time 

%[include cluster\_main\_list\_wct]
- Lists
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\footnotesize\tt
\input{results/cluster_main_list_wct.tex_incomplete}
}

%[include cluster\_main\_tree\_wct]
- Trees
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\footnotesize\tt
\input{results/cluster_main_tree_wct.tex}
}

{
\input{results/charts_main.tex}
}

Variable work time 

%[include cluster\_main\_list\_wvar]
- Lists
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\small\tt
\input{results/cluster_main_list_wvar.tex}
}

%[include cluster\_main\_tree\_wvar]
- Trees
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\small\tt
\input{results/cluster_main_tree_wvar.tex}
}

\subsubsection*{Test results based on our custom driver}
For these tests, basically, \textit{Sz} represents the number of \textit{RAdds} (removable adds) to make. The number of total operations in the workpool is obvious less or equal with 2 x number of RAdds (is less in case that some threads are still running RAdd operations but the others have passed already to the next step, remove-step).

Constant work time 

%[include cluster\_self\_list\_wct]
- Lists
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\footnotesize\tt
\input{results/cluster_self_list_wct.tex}
}

%[include cluster\_self\_tree\_wct]
- Trees
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\footnotesize\tt 
\input{results/cluster_self_tree_wct.tex}
}

Variable work time 

%[include cluster\_self\_list\_wvar]
- Lists
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\small\tt
\input{results/cluster_self_list_wvar.tex}
}

%[include cluster\_self\_tree\_wvar]
- Trees
\newline \newline
\renewcommand{\arraystretch}{1.2}
{\small\tt
\input{results/cluster_self_tree_wvar.tex}
}

\subsection{Analysis \& Comparison}

\subsubsection{Lists}
CoarseGrainedList, indifferently of the level of parallelism, performs well on smaller data set sizes, 16 - 160. Also, the same applies when the level of parallelism is low, 1,2 and in some cases 4 threads, indifferently of the size of the data sets. As for the rest of the larger data sets and higher contention, it begins to decrease in performance. However, in almost all of the cases it performs better than the FineGrainedList, which shows to be \textit{expensive} to use for a small size and low parallelism level (locks in general are quite expensive). On the other hand the FineGrainedList obtained good performance on larger data sets, 16k - 96k, with a high number of threads, 8 and 16. The LockFreeList implementation appears to have the best increase in performance as the problem size grows, list size as well as the level of parallelism. Therefore, it can be observed that the LockFreeList is the most scalable among them. Also, it deals great with a high contention because of its lock freeness. In consequence, on bigger data sets, 16k - 96k, it gains a significant advantage in execution time over the other implementations when using more than 8 threads. 

As a general conclusion for the list section, as we expected, CoarseGrainedList performs well on small datasets and low contention and FineGrainedList on a list large enough to support an increased number of threads and inner locking operations. LockFreeList balances well both parameters, size and contention level.  

\subsubsection{Trees}
Like in the previous case, the coarse grained implementation is the right choice for small data sets or small levels of parallelism. The CoarseGrainedTree performed well on problem sizes such as 160 - 1600 (independent of the number of threads) or with 1 - 2 threads (independent of the list size). As well as the FineGrainedList implementation the FineGrainedTree is also expensive when it comes to the locking mechanism. In almost all of the cases this implementation achieved poor results due to the fact that when we block the root of a subtree in order to search for the replacement node we might trigger a high contention 'spot'. Moreover, the search of the replacement node is done in a hand-over-hand fashion so it may take a while before we release the lock from above and settle the contention. As an exception, it would probably represent a good choice (in a equal-work distribution or a stealing-work paradigm) if we have just 2-4 threads and each one is 'working' on a separate branch of a large tree. The LockFreeTree implements a complex algorithm which uses more computational power than the other two, but makes good use of the parallelism provided by the processor and achieves a boost in performance for 4 or more threads. A major difference from the other implementations is that the structure of the tree is changed. We only operate on the bottom level (leaf nodes) and in addition with the lock free property no subtree root is going to represent a bottleneck (as in the FineGrainedTree). It represents an improvement because now all the treads can be scattered down the tree. Also, at that level more than one thread may contribute on helping another (i.e remove: flag-remove, mark-remove, physically-remove). 

\subsubsection{Lists vs. Trees}
Comparing lists vs. trees we can observe that trees are basically lists in two dimensions, therefore a smart algorithm can benefit of a higher level of parallelism just because of the structure (\textit{fan-out}). Though, we must be careful when we choose a locking mechanism: \textit{where}, \textit{when} and \textit{what} nodes are we trying to lock - the FineGrainedTree implementation does not take this into consideration and therefore achieves poor performance. Also, the tree structure permits efficient searches (up to $O(log n)$) which improves all our operations irrespective of their nature (ads or removes) and irrespective of the problem size or level of parallelism. As a comment, the FineGrainedTree implementation might be improved by selecting another method of synchronization that benefits from all of the above, such as the Optimistic or Lazy Synchronization. 
Besides the results from above, another practical comparison between the lists and trees can be seen in the following section where we perform a stress-test with many overlapping operations.  


Notes:

We must also consider the fact that in real applications \emph{add} and \emph{remove} operations are accompanied by some computation for performing actual work on the data items and it would be wrong to disregard this execution time. In our case the workTime parameter simulates this and we can see from the tests that it has an impact in some cases:
\begin{itemize}
\item For FineGrainedTree it eliminates some contention in cases where we have a high level of parallelism.
\item For the two lock-free implementation it just adds an overhead.
\end{itemize}

Most of the conclusions we have drawn are from the results of the tests done with the test driver provided in the assignment. We also included the results obtained with our test driver because there are some differences regarding execution times between the two test environments, and we think that this different approach of testing is also useful for determining the behaviour of our data structures in a real application.  


% Evaluation - CHAPTER_END

%% BREAK SECTION %%

% Stress Tests - CHAPTER_BEGIN
\section{Extra. Stress Test for Performance Relieve}
So, in the end for what purposes does somebody use these kind of data structures. Well, our 'intuition' is that we will want to store some objects in an ordered fashion, to perform operations in any order and preferable many of them in parallel, and probably we will not know anything in advance about what objects come first. To fit somehow this scenario we did a single overall stress-test using our custom driver (overlapping read/writes).
Test size: A=100k RA=50k R=100k. Time results are measured in milliseconds.

\subsection*{Platform: Magny-Cours}
\subsubsection*{Test results based on our custom driver}
\renewcommand{\arraystretch}{1.2}
{\small\tt
\begin{tabularx}{\linewidth}{cc|c|c|c|c|c|c|l|}
\cline{3-8}
& Wt & \multicolumn{6}{c|}{1} \\ 
\cline{3-8}
& Sz & \multicolumn{6}{c|}{100k,50k,100k} \\ 
\cline{3-8}
& Th & 2 & 4 & 8 & 16 & 32 & 64 \\ 
\cline{1-8}
\multicolumn{1}{|c}{cgl} &
\multicolumn{1}{|c|}{F} & 384855 & 409020 & 403461 & 429943 & 427943 & 429597 \\ 
\cline{1-8}
\multicolumn{1}{|c}{fgl} &
\multicolumn{1}{|c|}{F} & 560576 & 281970 & 155414 & 139824 & 97917 & 92979 \\ 
\cline{1-8}
\multicolumn{1}{|c}{lfl} &
\multicolumn{1}{|c|}{F} & 756721 & 374992 & 190903 & 104598 & 80510 & 77712 \\ 
\cline{1-8}
\multicolumn{1}{|c}{cgt} &
\multicolumn{1}{|c|}{F} & 609 & 890 & 904 & 935 & 826 & 882 \\  
\cline{1-8}
\multicolumn{1}{|c}{fgt} &
\multicolumn{1}{|c|}{F} & 581 & 594 & 1305 & 1377 & 1032 & 1016 \\ 
\cline{1-8}
\multicolumn{1}{|c}{lft} &
\multicolumn{1}{|c|}{F} & 520 & 293 & 300 & 321 & 329 & 465 \\ 
\cline{1-8}
\end{tabularx} }
\newline\newline
%Chart 
\begin{tikzpicture}
\begin{axis}[
	xlabel=$Th$,
	ylabel=$Time(ms)$]
\addplot+[smooth] coordinates
{(2, 384855) (4, 409020) (8, 403461) (16, 429943) (32, 427943) (64,429597)};
\addplot+[smooth] coordinates
{(2, 560576) (4, 281970) (8, 155414) (16, 139824) (32, 97917) (64, 92979)};
\addplot+[smooth] coordinates
{(2, 756721) (4, 374992) (8, 190903) (16, 104598) (32, 80510) (64, 77712)};
\legend{$cgl$\\$fgl$\\$lfl$\\}
\end{axis}
\end{tikzpicture}
\newline
\begin{tikzpicture}
\begin{axis}[
	xlabel=$Th$,
	ylabel=$Time(ms)$]
\addplot+[smooth] coordinates
{(2, 609) (4, 890) (8, 904) (16, 935) (32, 826) (64, 882)};
\addplot+[smooth] coordinates
{(2, 581) (4, 594) (8, 1305) (16, 1377) (32, 1032) (64, 1016)};
\addplot+[smooth] coordinates
{(2, 520) (4, 293) (8, 300) (16, 321) (32, 329) (64, 465)};
\legend{$cgt$\\$fgt$\\$lft$\\}
\end{axis}
\end{tikzpicture}

% Stress Tests - CHAPTER_END

%% BREAK SECTION %%
