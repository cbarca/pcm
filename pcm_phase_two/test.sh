#!/bin/bash


###### test types ######

simpleTest() {
  tmp=$file".tmp"
  results=$file".results"

  cat /dev/null > $file
  cat /dev/null > $tmp
  cat /dev/null > $results

# parameters for the program
  dstructs=""
#  dstructs+=" cgl"     
#  dstructs+=" fgl"
#  dstructs+=" lfl"
  dstructs+=" cgt"
  dstructs+=" fgt"
  dstructs+=" lft"

  nrThreads=""
  nrThreads+=" 2"
  nrThreads+=" 4"
  nrThreads+=" 6"
  nrThreads+=" 8"

  nrItems=""
  nrItems+=" 24" 
  nrItems+=" 240" 
  nrItems+=" 2400" 
  nrItems+=" 24000"
  nrItems+=" 240000"
#  nrItems+=" 2400000"

  workTime="10"

# number of times each test is done
  reps=5

  for instance in $dstructs           # implementation type
  do
    echo -e "\n\t\tTESTING <<$instance>> DATA STRUCTURE" >> $file
    echo "TESTING <<$instance>> DATA STRUCTURE" >> $results

    for threads in $nrThreads         # number of threads
    do
      echo -e "\tnrTh: "$threads >> $results

      for items in $nrItems           # number of items
      do
        echo -e "\t\tnrItems: "$items >> $results

        for work in $workTime         # work duration
        do
          echo -e "\t\t\twork: "$work >> $results
          echo -e "\n|----------" >> $file
          avrTime=0

          for (( i=1; i <= $reps; i++ ))
          do
            java -jar pcm.jar  $instance $threads $items $work  > $tmp

            t=`grep "time" $tmp | cut -d ' ' -f2` 
            avrTime=$(($avrTime + $t))

            count=`grep -c "\[\]" $tmp`
            if [ "$count" -ne 1 ]
            then
              echo "ERROR: "$instance >> $file
              echo "ERROR: "$instance
            fi

            sed -i '/^$/d' $tmp
            sed -e 's/^/| /' $tmp >> $file

            if [ "$i" -ne "$reps" ] 
            then
              echo "|*" >> $file
            fi
          done
          echo "|----------" >> $file
          echo "Total Time: "$avrTime" ms">> $file
          avrTime=$(($avrTime/$reps))
          echo "Average Time: "$avrTime" ms" >> $file
          echo "" >> $file
          echo -e "\t\t\tAverage Time(on a sample of "$reps"): "$avrTime" ms" >> $results
          echo "" >> $results
          echo "Finished <"$instance"> test with "$items" items and "$threads" threads."
        done
      done
    done
  echo "" >> $results
  done

  rm $tmp
}
###################################################


###### parsing args ######

usage() {
cat << EOF

Just run: $0 and see the content of 'output.results'.

usage: $0 options

OPTIONS:
-h	 Show this message
-t<type> Test type can be 'v'(validation), 'p'(performance) or 's'(simple)
-f<file> Output file
EOF
}

file="output"
testType="s"

while getopts "ht:f:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      testType=$OPTARG
      ;;
    f)
      file=$OPTARG
      ;;
    ?)
      usage
      exit 1
      ;;
  esac 
done
###################################################


###### do tests ######
case $testType in
  s)
    simpleTest
    ;;
  v)
    echo "[error] no validation  test"
    ;;
  p)
    echo "[error] no perfrmance test"
    ;;

esac

