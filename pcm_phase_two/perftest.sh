#!/bin/bash

    ##########################
    ######  test types  ######
    ##########################

perfTest() {
  tmp=$file".tmp"
  results=$file".short"

  cat /dev/null > $file
  cat /dev/null > $tmp
  cat /dev/null > $results

    ### parameters for the program ###

  # Uncomment data structures you want to test
  dstructs=""
  dstructs+=" cgl"     
  dstructs+=" fgl"
  dstructs+=" lfl"
  dstructs+=" cgt"
  dstructs+=" fgt"
  dstructs+=" lft"

  # Uncomment one ore more lines to run tests 
  # with different numbers of threads
  nrThreads=""
  nrThreads+=" 2"
  nrThreads+=" 4"
  nrThreads+=" 6"
  nrThreads+=" 8"

  # Uncomment one ore more lines to run tests 
  # with different amounts of work
  # Add			              # Removable Adds	      # Removes
  nrAdds=""		            nrRAdds=""		          nrRmvs=""	              nrWorkLoad=0
  nrAdds+=" 0000000"		  nrRAdds+=" 0001000"		  nrRmvs+=" 0000000"	    nrWorkLoad=$(($nrWorkLoad + 1))
#  nrAdds+=" 0000002"		  nrRAdds+=" 0000020"		  nrRmvs+=" 0000200"	    nrWorkLoad=$(($nrWorkLoad + 1))
#  nrAdds+=" 0000003"		  nrRAdds+=" 0000030"		  nrRmvs+=" 0000300"	    nrWorkLoad=$(($nrWorkLoad + 1))
#  nrAdds+=" 0000004"		  nrRAdds+=" 0000040"		  nrRmvs+=" 0000400"	    nrWorkLoad=$(($nrWorkLoad + 1))

  # Other parameters  
  allowDup="true"
#  allowDup="false"
  reps=5 # number of times each test is done
  workTime="1"

  for instance in $dstructs           # implementation type
  do
    echo -e "\n\t\tTESTING <<$instance>> DATA STRUCTURE" >> $file
    echo "TESTING <<$instance>> DATA STRUCTURE" >> $results

    for threads in $nrThreads         # number of threads
    do
      echo -e "\tnrTh: "$threads >> $results

      nrAdds2=$nrAdds nrRAdds2=$nrRAdds nrRmvs2=$nrRmvs
      for ((cnt=1; cnt<=nrWorkLoad; cnt++))           # number of items
      do
        nrAdd=`echo "$nrAdds2" | cut -d ' ' -f 2`
        nrAdds2=`echo "$nrAdds2" | sed 's/[^ ]* *\(.*\)$/\1/'`

        nrRAdd=`echo "$nrRAdds2" | cut -d ' ' -f 2`
        nrRAdds2=`echo "$nrRAdds2" | sed 's/[^ ]* *\(.*\)$/\1/'`

        nrRmv=`echo "$nrRmvs2" | cut -d ' ' -f 2`
        nrRmvs2=`echo "$nrRmvs2" | sed 's/[^ ]* *\(.*\)$/\1/'`

        echo -e "\t\tnrAdds: \t"$nrAdd >> $results
        echo -e "\t\tnrRAdds: \t"$nrRAdd >> $results
        echo -e "\t\tnrRmvs: \t"$nrRmv >> $results
        totalOp=`echo "$nrAdd+$nrRAdd*2+$nrRmv" | bc`
        echo -e "\t\tTotal ops:"$totalOp >> $results

        for work in $workTime         # work duration
        do
          echo -e "\t\t\twork: "$work >> $results
          echo -e "\n|----------" >> $file
          avrTime=0

          for (( i=1; i <= $reps; i++ ))
          do
            #echo $instance" "$threads" "$nrAdd" "$nrRAdd" "$nrRmv" "$work" "$allowDup
            java -jar pcm_perf.jar $instance $threads $nrAdd $nrRAdd $nrRmv $work $allowDup  > $tmp

            t=`grep "time" $tmp | cut -d ' ' -f2` 
            #avrTime=$(($avrTime + $t))
            avrTime=`echo "$avrTime + $t" | bc`

       #     count=`grep -c "\[\]" $tmp`
       #     if [ "$count" -ne 1 ]
       #     then
       #       echo "ERROR: "$instance >> $file
       #       echo "ERROR: "$instance
       #     fi

            sed -i '/^$/d' $tmp   # delete empy lines
            sed -e 's/^/| /' $tmp >> $file  # place '|' at the beginning of the line

            if [ "$i" -ne "$reps" ] 
            then
              echo "|*" >> $file
            fi
          done
          echo "|----------" >> $file
          echo "Total Time: "$avrTime" ms">> $file
          avrTime=$(($avrTime/$reps))
          echo "Average Time: "$avrTime" ms" >> $file
          echo "" >> $file
          echo -e "\t\t\tAverage Time(on a sample of "$reps"): "$avrTime" ms" >> $results
          echo "" >> $results
          echo "Finished <"$instance"> test with "$totalOp" items and "$threads" threads."
        done
      done
    done
  echo "" >> $results
  done

  rm $tmp
}
###################################################


    ###################################
    ######  parsing script args  ######
    ###################################

usage() {
cat << EOF

Just run: $0 and see the content of 'output.results'.

usage: $0 options

OPTIONS:
-h	 Show this message
-t<type> Test type can be 'p'(performance), 'v'(validation) or 's'(simple)
-f<file> Output file
EOF
}

file="results.out"
testType="p"

while getopts "ht:f:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 0
      ;;
    t)
      testType=$OPTARG
      ;;
    f)
      file=$OPTARG
      ;;
    ?)
      usage
      exit 1
      ;;
  esac 
done
###################################################


    ########################
    ######  do tests  ######
    ########################

case $testType in
  s)
    echo "[error] at the moment there is no simple test"
    ;;
  v)
    echo "[error] at the moment there is no validation test"
    ;;
  p)
    perfTest
    ;;

esac

