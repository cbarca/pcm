package pcm.trees;

import pcm.Sorted;

/**
 * Class LockFreeTree
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class LockFreeTree<T extends Comparable<T>> implements Sorted<T> {
	private LockFreeInternal<T> _root = null;
	private LockFreeLeaf<T>	_leafPlusInfinity = null, _leafMinusInfinity = null;
	
	/**
	 * Inner-class Window.
	 */
	private class Window {
		public LockFreeInternal<T> parent = null, gparent = null;
		public LockFreeLeaf<T> leaf = null;
		public Update<T> pupdate = null, gpupdate = null;
		
		public Window(LockFreeInternal<T> gp, 
				LockFreeInternal<T> p, LockFreeLeaf<T> l, 
				Update<T> pupdate, Update<T> gpupdate) {
			this.gparent = gp;
			this.parent = p;
			this.leaf = l;
			this.pupdate = pupdate;
			this.gpupdate = gpupdate;
		}
	}
	
	public LockFreeTree() {
		// Create three sentinels.
		_root = new LockFreeInternal<T>();
		_root.setAsMinusInfinty();
		
		_leafPlusInfinity = new LockFreeLeaf<T>();
		_leafPlusInfinity.setAsPlusInfinty();
		
		_leafMinusInfinity = new LockFreeLeaf<T>();
		_leafMinusInfinity.setAsMinusInfinty();
		
		_root.compareAndSetRight(null, _leafPlusInfinity);
		_root.compareAndSetLeft(null, _leafMinusInfinity);
	}
	
	/**
	 * Finds a <<window>> inside which our element
	 * is found. 
	 * No RMW operations need.
	 * @param item = item to found
	 * @return a window that consists of:
	 * 		<gparent, parent, leaf, pupdate, gpupdate>
	 */
	private Window find(T item) {
		LockFreeInternal<T> gparent = null, parent = null;
		LockFreeTreeNode<T> leaf = _root, 
				temp = new LockFreeTreeNode<T>(item);
		Update<T> gpupdate = null, pupdate = null;
		
		while (!(leaf instanceof LockFreeLeaf)) {
			gparent = parent;
			parent = (LockFreeInternal<T>) leaf;
			gpupdate = pupdate;
			pupdate = parent.getUpdate();
			
			if (temp.compareTo(leaf) < 0) {
				leaf = parent.getLeft();
			}
			else {
				leaf = parent.getRight();
			}
		}
		
		return new Window(gparent, parent, 
				(LockFreeLeaf<T>)leaf, 
				pupdate, gpupdate);
	}
		
	/**
	 * Add an item as a leaf in the tree 
	 * [operates on the last level].
	 * Non blocking method.
	 * @param item = value to insert as a node
	 */
	@Override
	public void add(T item) { 
		LockFreeInternal<T> newInternal = null;
		LockFreeLeaf<T> newSibling = null,
				newLeaf = new LockFreeLeaf<T>(item);
		AInfo<T> addInfo = null;
		Update<T> addUpdate = null;
		
		while (true) {
			Window w = find(item);
			
			if (w.leaf.compareTo(newLeaf) == 0) {
				return; // No duplicates.
			}
			
			if (!(w.pupdate.getState().equals(State.CLEAN))) {
				help(w.pupdate); // No CLEAN flag, try to help.
			}
			else {
				// Create a sibling for the new leaf.
				newSibling = new LockFreeLeaf<T>(w.leaf.getItem());
				// Extract the maximum value (between new and ex leaf), 
				// the value that will stand for guidance purposes.
				T maxItem = (w.leaf.compareTo(newLeaf) > 0) ? w.leaf.getItem() : item;
				// Create the new internal node with the above max value.
				newInternal = new LockFreeInternal<T>(maxItem); 
				newInternal.compareAndSetUpdate(null, new Update<T>(State.CLEAN, null));
			
				// Connect the new leaf + new sibling to the
				// new internal node.
				if (newLeaf.compareTo(newSibling) < 0) {
					newInternal.compareAndSetLeft(null, newLeaf);
					newInternal.compareAndSetRight(null, newSibling);
				} else {
					newInternal.compareAndSetRight(null, newLeaf);
					newInternal.compareAndSetLeft(null, newSibling);
				}
				
				// Prepare the AddUpdate CASWord.
				addInfo = new AInfo<T>(w.parent, w.leaf, newInternal);
				addUpdate = new Update<T>(State.AFLAG, addInfo);
				
				// AddUpdate CAS.
				if (w.parent.compareAndSetUpdate(w.pupdate, addUpdate)) {
					// Finish add.
					helpAdd(addInfo, addUpdate);
				}
				else {
					// Try to help.
					help(w.parent.getUpdate());
				}
			}
		}
	}

	/**
	 * Helper for the Add method, add step.
	 * @param addInfo = current add's information
	 * @param addUpdate = the expected Update reference to have
	 */
	private void helpAdd(AInfo<T> addInfo, Update<T> addUpdate) {
		if (addInfo == null) {
			return;
		}
		
		// Add child CAS.
		casChild(addInfo.getParent(), addInfo.getLeaf(), addInfo.getNewInternal());
		// CLEAN AddUpdate CAS.
		addInfo.getParent().compareAndSetUpdate(addUpdate, 
				new Update<T>(State.CLEAN, addInfo));
	}
	
	/**
	 * Remove the item from the tree
	 * [operates on the last level].
	 * Non blocking method.
	 * @param item = value to be removed
	 */
	@Override
	public void remove(T item) {
		Window w = null;
		RInfo<T> removeInfo = null;
		Update<T> removeUpdate = null;	
		LockFreeLeaf<T> temp = new LockFreeLeaf<T>(item);
	
		while (true) {
			w = find(item);
			
			if (w.leaf.compareTo(temp) != 0) {
				return; // No item found.
			}
			
			if (!(w.gpupdate.getState().equals(State.CLEAN))) {
				help(w.gpupdate); // No CLEAN flag, try to help.
			}
			else if (!(w.pupdate.getState().equals(State.CLEAN))) {
				help(w.pupdate); // No CLEAN flag, try to help.
			}
			else {
				// Prepare the RemoveUpdate CASWord.
				removeInfo = new RInfo<T>(w.gparent, w.parent, w.leaf, w.pupdate);
				removeUpdate = new Update<T>(State.RFLAG, removeInfo);
				
				// RemoveUpdate CAS.
				if (w.gparent.compareAndSetUpdate(w.gpupdate, removeUpdate)) {
					// Proceed to mark.
					helpRemove(removeInfo, removeUpdate);
				}
				else {
					// Try to help.
					help(w.gparent.getUpdate());
				}
			}
		}
	}
	
	/**
	 * Helper for the Remove method, mark step.
	 * @param removeInfo = current remove's information
	 * @param removeUpdate = the expected Update reference to have
	 */
	private void helpRemove(RInfo<T> removeInfo, Update<T> removeUpdate) {
		Update<T> markUpdate = null;
		
		if (removeInfo == null) {
			return;
		}
		
		// Prepare the MarkUpdate CASWord.
		markUpdate = new Update<T>(State.MARK, removeInfo);
		
		// MarkUpdate CAS.
		if (removeInfo.getParent().compareAndSetUpdate(
				removeInfo.getParentUpdate(), markUpdate) || 
			removeInfo.getParent().getUpdate().getState().equals(State.MARK)) {
			// Finish remove.
			helpMark(removeInfo, removeUpdate);
		}
		else {
			// MarkUpdate CAS failed.
			// Help the operation that caused the failure.
			help(removeInfo.getParent().getUpdate());
			// Backtrack CAS: Undo RemoveUpdate CAS (back to CLEAN).
			removeInfo.getGrandparent().compareAndSetUpdate(removeUpdate, 
					new Update<T>(State.CLEAN, removeInfo));
			// We do not clean the mark flag -> the node 
			// will be deleted by the GC.
		}
	}
	
	/**
	 * Helper for the Remove method, remove step.
	 * @param removeInfo = current remove's information
	 * @param removeUpdate = the expected Update reference to have
	 */
	private void helpMark(RInfo<T> removeInfo, Update<T> removeUpdate) {
		LockFreeTreeNode<T> other = null;
		
		if (removeInfo == null) {
			return;
		}
		
		// Get the soon-to-be-removed node's sibling.
		if (removeInfo.getParent().getRight().
				compareTo(removeInfo.getLeaf()) == 0) {
			other = removeInfo.getParent().getLeft();
		}
		else {
			other = removeInfo.getParent().getRight();
		}
		
		// Remove child CAS.
		casChild(removeInfo.getGrandparent(), removeInfo.getParent(), other);
		// CLEAN RemoveUpdate CAS.
		removeInfo.getGrandparent().compareAndSetUpdate(removeUpdate, 
				new Update<T>(State.CLEAN, removeInfo));
	}
	
	private void help(Update<T> update) {
		// Help branch: Add, Remove, Mark
		if (update.getState().equals(State.AFLAG)) {
			helpAdd((AInfo<T>)update.getInfo(), update);
		}
		else if (update.getState().equals(State.RFLAG)) {
			helpRemove((RInfo<T>)update.getInfo(), update);
		}
		else if (update.getState().equals(State.MARK)) {
			helpMark((RInfo<T>)update.getInfo(), update);
		}
	}
	
	/**
	 * CAS child exchange. 
	 * @param parent = above parent
	 * @param oldn = old node
	 * @param newn = new node 
	 */
	private void casChild(LockFreeInternal<T> parent, 
			LockFreeTreeNode<T> oldn, LockFreeTreeNode<T> newn) {
		// Exchange old child with new child.
		if (newn.compareTo(parent) < 0) {
			parent.compareAndSetLeft(oldn, newn);
		}
		else {
			parent.compareAndSetRight(oldn, newn);
		}
	}
	
	private String infix(LockFreeTreeNode<T> node) {
		if (node instanceof LockFreeLeaf) {
			if (node.getItem() != null)
				return node.getItem().toString() + ",";
			else
				return "";
		}
		
		else {
			return 	infix( ((LockFreeInternal<T>)node).getLeft() ) +
					infix( ((LockFreeInternal<T>)node).getRight() );
		}
	}
	
	@Override
	public String toString() {
		String s = infix(_root);
		
		if (s.length() == 0)
			return "[]";
		else 
			return "[" + s.substring(0, s.length() - 1) + "]";
	}
}
