package pcm.trees;

enum State {
	CLEAN, RFLAG, AFLAG, MARK;
}

/**
 * Class Update.
 * Two in one class, keeps state + info references 
 * for the AtomicReference.
 * 
 * @author Cristian Barca - cba390
 * @author Livu Razorea - lra230
 * 
 * @param <T extends Comparable<T>>
 */
public class Update<T extends Comparable<T>>  {
	// _Private members
	private State _state = null;
	private Info<T> _info = null;
	
	public Update() {
		this(null, null);
	}
	
	public Update(State state, Info<T> info) {
		_state = state;
		_info = info;
	}
	
	public State getState() {
		return _state;
	}
	
	public Info<T> getInfo() {
		return _info;
	}

	public void setState(State state) {
		_state = state;
	}
 	
	public void setInfo(Info<T> info) {
		_info = info;
	}
	
	@Override
	public boolean equals(Object update) {
		@SuppressWarnings("unchecked")
		Update<T> tUpdate = (Update<T>) update;
		return (_state.equals(tUpdate.getState()) && 
				_info.equals(tUpdate.getInfo()));
	}
}
