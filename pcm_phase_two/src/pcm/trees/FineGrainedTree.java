package pcm.trees;

import java.security.InvalidParameterException;
import pcm.Sorted;

/**
 * Class FineGrainedTree
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 * 
 * @param <T> extends Comparable<T>
 */
public class FineGrainedTree <T extends Comparable<T>> implements Sorted<T>{
	private LockTreeNode<T> root = null;
	
	public FineGrainedTree() {
		root = new LockTreeNode<T>(null);
	}
	
	/**
	 * Add an item as a leaf node in the tree.
	 * Fine grained blocking method.
	 * @param item = value to insert as a node
	 */
	public void add(T item) {
		LockTreeNode<T> pred;
		
		root.lock();
		pred = root;
		try {
			if (root.getLeft() == null) {
				root.setLeft(new LockTreeNode<T>(item));
				return;
			}
			
			// always go left (because root is infinity)
			LockTreeNode<T> next, curr = pred.getLeft();
			curr.lock();
			try {
				
				do {
					if (curr.getItem().compareTo(item) > 0) {
						next = curr.getLeft();
					}
					else {
						next = curr.getRight();
					}
					
					if (next != null) { // still need to advance 'pred'
						pred.unlock();	// and 'curr'
						pred = curr;
						curr = next;
						curr.lock();
					}
					else { 	/* add the new item as a leaf node and exit
							 * the loop */
						if (curr.getItem().compareTo(item) > 0) {
							curr.setLeft(new LockTreeNode<T>(item));
						}
						else {
							curr.setRight(new LockTreeNode<T>(item));
						}
						
						return;
					}	
				} while (true);
				
			} finally {
				curr.unlock();
			}
			
		} finally {
			pred.unlock();
		}
	}

	/**
	 * Removes 'node' from the tree.
	 * The method only works if 'node' has at most one child.
	 * The method was devised to serve as a helper function for implementing
	 * 'remove(T item)'. It has limited functionality and should not be used
	 * anywhere else.
	 * @param pred = the parent of 'node'
	 * @param node = the node to be removed
	 */
	private void limitedDelete(LockTreeNode<T> pred, LockTreeNode<T> node) {
		LockTreeNode<T> s;
		
		if (node.has2subTrees()) {
			throw new InvalidParameterException();
		}
		
		if (node.hasLeft()) {
			s = node.getLeft();
		}
		else {
			s = node.getRight();
		}
		
		if (pred.getLeft() == node) {
			pred.setLeft(s);
		}
		else if (pred.getRight() == node) {
			pred.setRight(s);
		}
		else {
			throw new InvalidParameterException();
		}
	}
	
	
	/**
	 * Removes the node from the tree that equals item.
	 * Fine grained blocking method.
	 * @param item = value to delete
	 */
	public void remove(T item) {
		LockTreeNode<T> pred;
		
		root.lock();
		pred = root;
		try {
			if (!root.hasLeft()) {
				return;
			}
			
			LockTreeNode<T> next, oldCurr = null,
					curr = pred.getLeft(); // always go left (because root is infinity)
			curr.lock();
			try {
				
				do {
					if (curr.getItem().compareTo(item) > 0) {
						next = curr.getLeft();
					}
					else if (curr.getItem().compareTo(item) < 0) {
						next = curr.getRight();
					}
					else {
						if (curr.has2subTrees()) {
							/* The node to be removed has two children. We need 
							 * to change it so it has at most one child:
							 * 
							 * 1. keep the current node locked and a 
							 * reference to it in oldCurr
							 * 
							 * 2. find the largest item smaller than the current, and
							 * its parent (curr and pred)
							 * 
							 * 3. move the item from curr to oldCurr
							 * 
							 * 4. now we only have to delete a node which has 
							 * at most one child
							 */
							
							oldCurr = curr;
							curr = curr.getLeft();
							curr.lock();
							try {
								if (!curr.hasRight()) {
									pred.unlock();
									pred = oldCurr;
									pred.lock();
								}
								
								while (curr.hasRight()) {
									pred.unlock();
									pred = curr;
									curr = curr.getRight();
									curr.lock();
								}
								
								oldCurr.setItem(curr.getItem());
							} finally {
								oldCurr.unlock();
							}
						}
						
						/* Remove the node 'curr' */
						limitedDelete(pred, curr);
						break; // exit the infinite loop
					}
					
					if (next == null) {
						//throw new NoSuchElementException();
						return;
					}
					else {
						pred.unlock();
						pred = curr;
						curr = next;
						curr.lock();
					}
				} while (true);
				
			} finally {
				curr.unlock();
			}
		} finally {
			pred.unlock();
		}
		
	}
	
	
	/**
	 * Traverses the tree in infix order and returns the
	 * list of items in string form.
	 * @param r = the root node
	 */
	private String infix(LockTreeNode<T> r) {
		/* Print the item in r */
		String s = "";
		
		if (r == null)
			return "";
			
		if (r.hasLeft()) {
			s = infix(r.getLeft()) + ",";
		}
		
		s = s + r.getItem();
		
		if (r.hasRight()) {
			s = s + "," +infix(r.getRight());
		}
		
		return s;

	}
	
	
	/**
	 * Returns a string representation of the tree.
	 * It should be used only for debugging purposes and
	 * must remain a private member.
	 * @param r = the root node
	 * @param d = indentation depth (should be 0)
	 */
	private String treeDump(LockTreeNode<T> r, int d) {
		String tab ="";
		for (int i = 0; i < d; i++)
			tab = tab + " ";
		
		if (r == null)
			return tab + "-> --\n";
		else 
			return tab + "-> " + r.getItem() + "\n" 
				+ treeDump(r.getLeft(), d + 4)
				+ treeDump(r.getRight(), d + 4);
	}
	
	
	/**
	 * A wrapper to make the previous method public.
	 */
	public String treeDump() {
		return treeDump(root, 0);
	}
	
	
	/**
	 * Print the tree in infix order.
	 * <T> should also implement the toString() method.
	 */
	public String toString() {
		return "[" + infix(root.getLeft()) + "]";
	
	}
}

