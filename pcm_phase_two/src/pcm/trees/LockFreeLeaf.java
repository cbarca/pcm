package pcm.trees;

/**
 * Class LockFreeLeaf.
 *  
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T extends Comparable<T>>
 */
public class LockFreeLeaf<T extends Comparable<T>> extends LockFreeTreeNode<T> {
	
	public LockFreeLeaf() {
		super(null);
	}
	
	public LockFreeLeaf(T item) {
		super(item);
	}
}
