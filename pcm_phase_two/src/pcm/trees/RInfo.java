package pcm.trees;

/**
 * Class RInfo.
 * Contains information for remove operations.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T extends Comparable<T>>
 */
public class RInfo<T extends Comparable<T>> extends Info<T> {
	// _Private members
	private LockFreeInternal<T> _p = null, _gp = null;
	private Update<T> _pupdate;
	
	public RInfo() {
		this(null, null, null, null);
	}
	
	public RInfo(LockFreeInternal<T> gp, LockFreeInternal<T> p, LockFreeLeaf<T> leaf, Update<T> pupdate) {
		super(leaf);
		
		_gp = gp;
		_p = p;
		_pupdate = pupdate;
	}
	
	
	public LockFreeInternal<T> getParent() {
		return _p;
	}
	
	public LockFreeInternal<T> getGrandparent() {
		return _gp;
	}
	
	public Update<T> getParentUpdate() {
		return _pupdate;
	}
}
