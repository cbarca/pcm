package pcm.trees;

/**
 * Class LockFreeTreeNode.
 * Base class for LockFreeTree nodes. 
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T extends Comparable<T>
 */
public class LockFreeTreeNode<T extends Comparable<T>> implements Comparable<LockFreeTreeNode<T>> {
	// _Private members
	private T _item = null;
	private boolean _plusInfinity = false, _minusInfinity = false;

	public LockFreeTreeNode() {
		this(null);
	}
	
	public LockFreeTreeNode(T item) {
		_item = item;
	}
	
	public boolean isPlusInfinity() {
		return _plusInfinity;
	}
	
	public boolean isMinusInfinity() {
		return _minusInfinity;
	}
	
	public void setAsPlusInfinty() {
		_plusInfinity = true;
	}
	
	public void setAsMinusInfinty() {
		_minusInfinity = true;
	}
	
	public T getItem() {
		return _item;
	}
	
	public void setItem(T item) {
		_item = item;
	}
	
	@Override
	public int compareTo(LockFreeTreeNode<T> node) {
		if (_item == null && node.getItem() == null) {
			return 0;
		}
		
		if (_item == null) {
			return ((_minusInfinity) ? (-1) : (1));
		}
		
		if (node.getItem() == null) {
			return ((node.isMinusInfinity()) ? (1) : (-1));
		}
		
		return _item.compareTo(node.getItem());
	}
}
