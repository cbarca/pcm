package pcm.trees;

/**
 * Class AInfo.
 * Contains information for add operations.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T extends Comparable<T>>
 */
public class AInfo<T extends Comparable<T>> extends Info<T> {
	// _Private members
	private LockFreeInternal<T> _p = null, _newInternal = null;
	
	public AInfo() {
		this(null, null, null);
	}
	
	public AInfo(LockFreeInternal<T> p, LockFreeLeaf<T> leaf ,LockFreeInternal<T> newInternal) {
		super(leaf);
		
		_p = p;
		_newInternal = newInternal;
	}
	
	
	public LockFreeInternal<T> getParent() {
		return _p;
	}
	
	public LockFreeInternal<T> getNewInternal() {
		return _newInternal;
	}
}
