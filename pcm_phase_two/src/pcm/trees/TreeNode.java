package pcm.trees;

/**
 * TreeNode class - node class for CoarseGrainedTree
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class TreeNode<T extends Comparable<T>> implements Comparable<TreeNode<T>>{
	protected T _item;
	protected TreeNode<T> _left;
	protected TreeNode<T> _right;
	
	public TreeNode() {
		throw new UnsupportedOperationException();
	}
	
	public TreeNode(T item) {
		_item = item;
		_left = null;
		_right = null;
	}
	
	public T getItem() {
		return _item;
	}
	
	public void setItem(T item) {
		_item = item;
	}
	
	public TreeNode<T> getLeft() {
		return _left;
	}
	
	public void setLeft(TreeNode<T> left) {
		_left = left;
	}
	
	public boolean hasLeft() {
		return _left != null;
	}
	
	public TreeNode<T> getRight() {
		return _right;
	}
	
	public void setRight(TreeNode<T> right) {
		_right = right;
	}
	
	public boolean hasRight() {
		return _right != null;
	}

	public boolean has2subTrees() {
		return hasLeft() && hasRight();
	}
	
	@Override
	public int compareTo(TreeNode<T> node) {
		return _item.compareTo(node.getItem());
	}
}
