package pcm.trees;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import pcm.*;

/**
 * Class CoarseGrainedTree
 * 
 * @author Cristian Barca - cba390	
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class CoarseGrainedTree<T extends Comparable<T>> implements Sorted<T> {
	private TreeNode<T> root;
	private Lock lock;
	
	
	public CoarseGrainedTree() {
		root = null;
		lock = new ReentrantLock();
	}
	
	
	/**
	 * Adds an item as a leaf node in the tree.
	 * Coarse blocking method.
	 * @param item = value to insert as a node
	 */
	public void add(T item) {
		TreeNode<T> curr, pred;
		
		lock.lock();
		try {
			
			if (root == null) {
				root = new TreeNode<T>(item);
				return;
			}
			
			curr = root;
			
			do {
				pred = curr;
				
				if (pred.getItem().compareTo(item) > 0)
					curr = curr.getLeft();
				else
					curr = curr.getRight();
			} while (curr != null);
			
			if (pred.getItem().compareTo(item) > 0)
				pred.setLeft(new TreeNode<T>(item));
			else
				pred.setRight(new TreeNode<T>(item));
			
		} finally {
			lock.unlock();
		}
	}
	
	
	/**
	 * Removes the node from the tree that equals item.
	 * Coarse blocking method.
	 * @param item = value to delete
	 */
	public void remove(T item) {
		TreeNode<T> curr = root, pred = null;
		TreeNode<T> s, ps;
		
		lock.lock();
		try {
			
			while (curr != null && curr.getItem().compareTo(item) != 0) {
				pred = curr;
				curr = curr.getItem().compareTo(item) > 0 ? curr.getLeft() : curr.getRight();
			}
			
			if (curr == null)
				return;
			
			if (curr.has2subTrees()) {
				/* find largest item smaller than curr and
				 * its parent (s and ps) */
				ps = curr;
				s = ps.getLeft();
				while (s.getRight() != null) {
					ps = s;
					s = s.getRight();
				}
				
				// put s in place of curr
				curr.setItem(s.getItem());
				curr = s;
				pred = ps;
			}
			
			/* curr has at most one child and s will hold
			 *  a reference to that child (if it exists) */
			if (curr.getLeft() != null)
				s = curr.getLeft();
			else 
				s = curr.getRight();
			
			if (curr == root) 
				root = s;
			else {
				if (pred.getLeft() == curr)
					pred.setLeft(s);
				else 
					pred.setRight(s);
			}
			
			curr = null;
			
		} finally {
			lock.unlock();
		}
	}
	
	
	/**
	 * Traverses the tree in infix order and returns the
	 * list of items in string form.
	 * @param r = the root node
	 */
	private String infix(TreeNode<T> r) {
		/* Print the item in r */
		String s = "";
		
		if (r == null)
			return "";
		
		if (r.hasLeft()) {
			s = infix(r.getLeft()) + ",";
		}
		
		s = s + r.getItem();
		
		if (r.hasRight()) {
			s = s + "," +infix(r.getRight());
		}
		return s;
	}
	
	
	/**
	 * Returns a string representation of the tree.
	 * It should be used only for debugging purposes and
	 * must remain a private member.
	 * @param r = the root node
	 * @param d = indentation depth (should be 0)
	 */
	private String treeDump(TreeNode<T> r, int d) {
		String tab ="";
		for (int i = 0; i < d; i++)
			tab = tab + " ";
		
		if (r == null)
			return tab + "-> --\n";
		else 
			return tab + "-> " + r.getItem() + "\n" 
				+ treeDump(r.getLeft(), d + 4)
				+ treeDump(r.getRight(), d + 4);
	}
	
	
	/**
	 * A wrapper to make the previous method public.
	 */
	public String treeDump() {
		return treeDump(root, 0);
	}
	
	/**
	 * Prints the tree in infix order.
	 * <T> should also implement the toString() method.
	 */
	@Override
	public String toString() {
		return "[" + infix(root) + "]";
	}
}
