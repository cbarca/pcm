package pcm.trees;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LockTreeNode class - node class for FineGrainedTree
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class LockTreeNode<T extends Comparable<T>> extends TreeNode<T> {
	private Lock lock;
	
	public LockTreeNode(T item) {
		super(item);
		lock = new ReentrantLock();
	}
	
	public LockTreeNode<T> getLeft() {
		return (LockTreeNode<T>)_left;
	}
	
	public void setLeft(LockTreeNode<T> left) {
		_left = left;
	}
	
	public LockTreeNode<T> getRight() {
		return (LockTreeNode<T>)_right;
	}
	
	public void setRight(LockTreeNode<T> right) {
		_right = right;
	}
	
	/**
	 * Locks tree node
	 */
	void lock() {
		lock.lock();
	}
	
	/**
	 * Unlocks tree node
	 */
	void unlock() {
		lock.unlock();
	}
}
