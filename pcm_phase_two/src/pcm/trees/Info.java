package pcm.trees;

/**
 * Class Info.
 * Base class for Remove/Add Info.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T extends Comparable<T>
 */
public class Info<T extends Comparable<T>> {
	// _Private members
	LockFreeLeaf<T> _leaf = null;
	
	public Info() {
		this(null);
	}
	
	public Info(LockFreeLeaf<T> leaf) {
		_leaf = leaf;
	}
	
	public LockFreeLeaf<T> getLeaf() {
		return _leaf;
	}
	
	public void setLeaf(LockFreeLeaf<T> leaf) {
		_leaf = leaf;
	}
	
	@Override
	public boolean equals(Object info) {
		@SuppressWarnings("unchecked")
		Info<T> tInfo = (Info<T>) info;
		return (_leaf.getItem().equals(tInfo.getLeaf().getItem()));
	}
}
