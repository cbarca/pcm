package pcm.trees;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Class LockFreeInternal.
 * Internal node of the LockFreeTree - 'guidance' nodes.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razoiera - lra230
 *
 * @param <T extends Comparable<T>>
 */
public class LockFreeInternal<T extends Comparable<T>> 
	extends LockFreeTreeNode<T> {
	// _Private members
	AtomicReference<Update<T>> _update = null;
	AtomicReference<LockFreeTreeNode<T>> _left = null, _right = null;
	
	public LockFreeInternal() {
		this(null);
	}
	
	public LockFreeInternal(T item) {
		super(item);
		
		_update = new AtomicReference<Update<T>>(new Update<T>(State.CLEAN, null));
		_left = new AtomicReference<LockFreeTreeNode<T>>(null);
		_right = new AtomicReference<LockFreeTreeNode<T>>(null);
	}
	
	public LockFreeTreeNode<T> getRight() {
		return _right.get();
	}
	
	public LockFreeTreeNode<T> getLeft() {
		return _left.get();
	}
	
	public Update<T> getUpdate() {
		return _update.get();
	}
	
	public boolean compareAndSetUpdate(Update<T> expectedReference, 
			Update<T> newReference) {
		return _update.compareAndSet(expectedReference, newReference);
	}
	
	public boolean compareAndSetLeft(LockFreeTreeNode<T> expectedReference, 
			LockFreeTreeNode<T> newReference) {
		return _left.compareAndSet(expectedReference, newReference);
	}
	
	public boolean compareAndSetRight(LockFreeTreeNode<T> expectedReference, 
			LockFreeTreeNode<T> newReference) {
		return _right.compareAndSet(expectedReference, newReference);
	}
}
