package pcm.lists;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LockListNode class - node class for FineGrainedList
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class LockListNode<T extends Comparable<T>> extends ListNode<T> {
	// _Private members
	private Lock _lock = null;

	public LockListNode() {
		this(null);
	}
	
	public LockListNode(T item) {
		super(item);
		_lock = new ReentrantLock();
	}
	
	/**
	 * Lock method.
	 */
	public void lock() {
		_lock.lock();
	}
	
	/**
	 * Unlock method.
	 */
	public void unlock() {
		_lock.unlock();
	}
}
