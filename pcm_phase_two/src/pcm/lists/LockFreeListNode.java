package pcm.lists;

import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * LockFreeListNode class - node class for LockFreeTree
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class LockFreeListNode<T extends Comparable<T>> extends ListNode<T> { 
	// _Private members
	private AtomicMarkableReference<LockFreeListNode<T>> _next = null; 
	
	public LockFreeListNode() {
		this(null);
	}
	
	public LockFreeListNode(T item) {
		super(item);
		_next = new AtomicMarkableReference<LockFreeListNode<T>>(null, false);
	}

	@Override
	public LockFreeListNode<T> getNext() {
		return _next.getReference();
	}
	
	public LockFreeListNode<T> getNext(boolean[] marked) {
		return _next.get(marked);
	}
	
	@Override
	public void setNext(ListNode<T> next) {
		// NOT supported - instead of setNext we do compareAndSetNext
		throw new UnsupportedOperationException();
	}
	
	public boolean attemptMarkNext(LockFreeListNode<T> oldRef, boolean newMark)  {
		return _next.attemptMark(oldRef, newMark);
	}
	
	public boolean compareAndSetNext(LockFreeListNode<T> oldRef, LockFreeListNode<T> newRef, 
			boolean oldMark, boolean newMark) {
		return _next.compareAndSet(oldRef, newRef, oldMark, newMark);
	}	
}
