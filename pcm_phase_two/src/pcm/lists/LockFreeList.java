package pcm.lists;

import pcm.Sorted;

/**
 * Class LockFreeList
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class LockFreeList<T extends Comparable<T>> implements Sorted<T> {
	// _Private members.
	private LockFreeListNode<T> _head = null, _tail = null;
	
	public LockFreeList() {
		// Create two sentinels.
		_head = new LockFreeListNode<T>();
		_head.setAsMinusInfinty();
		
		_tail = new LockFreeListNode<T>();
		_tail.setAsPlusInfinty();
		
		_head.compareAndSetNext(null, _tail, false, false);
	}
	
	/**
	 * Inner-class Window.
	 */
	private class Window {
		// Public members.
		public LockFreeListNode<T> pred = null, curr = null;
		
		public Window(LockFreeListNode<T> pred, LockFreeListNode<T> curr) {
			this.pred = pred; 
			this.curr = curr;
		}		
	}
	
	/**
	 * Helper method - find item and return
	 * its window consisted of pred and curr.
	 * (on its way to find item we physically 
	 * delete marked nodes)
	 * Non blocking method.
	 * @param item = item to find
	 * @return Window object
	 */
	public Window find(T item) {
		LockFreeListNode<T> pred = null, curr = null, succ = null,
				temp = new LockFreeListNode<T>(item);
		boolean[] marked = { false };
		boolean snip, gotow;
		
		while (true) {
			pred = _head;
			curr = pred.getNext();
			gotow = true; // _goto: go to _while.
			
			while (gotow) { // _while
				succ = curr.getNext(marked);
				
				// Remove marked nodes, possibly a chain of them.
				while (marked[0]) {
					snip = pred.compareAndSetNext(curr, succ, false, false);
					
					if (!snip) {
						gotow = false;
						break;
					}
					
					curr = succ;
					succ = curr.getNext(marked);
				}
				
				if (!gotow) {
					break; // Go to _goto.
				}
				
				// We found a node >= temp.
				if (curr.compareTo(temp) >= 0) {
					return new Window(pred, curr);
				}
				
				pred = curr;
				curr = succ;
			}
		}
	}

	/**
	 * Add an item as a node in the list [up-to-front addition].
	 * Non blocking method.
	 * @param item = value to insert as a node
	 */
	@Override
	public void add(T item) {
		LockFreeListNode<T> pred = null, curr = null;
		
		while (true) {
			Window w = find(item);
			pred = w.pred; 
			curr = w.curr;
			
			LockFreeListNode<T> node = new LockFreeListNode<T>(item);
			node.compareAndSetNext(null, curr, false, false);
			
			// We attempt to insert our node.
			if (pred.compareAndSetNext(curr, node, false, false)) {
				return;
			}
		}
	}
	
	/**
	 * Remove the first node from the list that equals item.
	 * Non blocking method.
	 * @param item = value to delete from the list
	 */
	@Override
	public void remove(T item) {
		LockFreeListNode<T> pred = null, curr = null, succ = null,
				temp = new LockFreeListNode<T>(item);
		boolean snip;
		
		while (true) {
			Window w = find(item);			
			pred = w.pred; 
			curr = w.curr;
			
			if (curr.compareTo(temp) != 0) {
				 return;
			}
			
			succ = curr.getNext();
			// Mark curr for remove (there might be
			// some contention over here, but eventually
			// it will be solved at the CAS).
			snip = curr.attemptMarkNext(succ, true);
				
			if (!snip) {
				continue; // Retry if we failed at marking.
			}
			
			// NOTE: find() returns the first element equal with 'item'
			// and since the list has duplicates then is possible that
			// find() returns the same position in the list for 2 threads
			// that want to remove the same element. This is different
			// than the 'help' case, because now a remove failure doesn't
			// truly mean that another thread is helping us - it could mean
			// that the thread is removing it's own element. So, in case of
			// failure we must retry again to remove our element!
			if (pred.compareAndSetNext(curr, succ, false, false)) {				
				return;
			}
		}
	}
	
	/**
	 * Print the list without the dummy sentinels .
	 * <T> should also implement the toString() method.
	 */
	@Override
	public String toString() {
		LockFreeListNode<T> curr = null;
		boolean[] marked  = new boolean[1];
		String output;
		
		curr = _head.getNext();
		output = "[";
			
		while (curr.getNext(marked) != null) {
			if (!marked[0]) {
				output += curr.getItem();
			}
					
			curr = curr.getNext();
			
			if (curr.getNext() != null) {
				output += ",";
			}
		}
		
		output += "]";	
		
		return output;
	}
}
