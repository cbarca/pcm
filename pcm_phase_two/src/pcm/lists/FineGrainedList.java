package pcm.lists;

import pcm.Sorted;

/**
 * Class FineGrainedList 
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 * 
 * @param <T> extends Comparable<T>
 */
public class FineGrainedList<T extends Comparable<T>> implements Sorted<T> {
	// _Private members
	private LockListNode<T> _head = null, _tail = null;
		
	public FineGrainedList() {
		// Create two sentinels
		_head = new LockListNode<T>();
		_head.setAsMinusInfinty();
		
		_tail = new LockListNode<T>();
		_tail.setAsPlusInfinty();
		
		_head.setNext(_tail);
	}
	
	/**
	 * Add an item as a node in the list [up-to-front addition].
	 * Fine blocking method - hand-over-hand blocking / lock coupling.
	 * @param item = value to insert as a node
	 */
	@Override
	public void add(T item) {
		LockListNode<T> pred = null, curr = null,
				temp = new LockListNode<T>(item);
		
		// Lock head - hand(1)
		_head.lock();
		pred = _head;
		
		try { // over(2)
			curr = (LockListNode<T>) pred.getNext();
			
			// Lock curr - hand(3)
			curr.lock();
			
			try { // Enter critical section
			
				// Up-to-front lock-coupling find
				while (curr.compareTo(temp) < 0) {
					pred.unlock();
					pred = curr;
					curr = (LockListNode<T>) curr.getNext();
					curr.lock();
				}
		
				// Add item as a lock list node
				LockListNode<T> node = new LockListNode<T>(item);
				node.setNext(curr);
				pred.setNext(node);
			} finally {
				// Unlock curr
				curr.unlock();
			}
		} finally {
			// Unlock pred
			pred.unlock();		
		}
	}
	
	/**
	 * Remove the first node from the list that equals item.
	 * Fine blocking method - hand-over-hand blocking / lock coupling.
	 * @param item = value to delete from the list
	 */
	@Override
	public void remove(T item) {
		LockListNode<T> pred = null, curr = null,
				temp = new LockListNode<T>(item);
		
		// Lock head - hand(1)
		_head.lock();
		pred = _head;
		
		try { // over(2)
			curr = (LockListNode<T>) pred.getNext();
			
			// Lock curr - hand(3)
			curr.lock();
			
			try { // Enter critical section
			
				// Up-to-front lock-coupling find
				while (curr.compareTo(temp) < 0) {
					pred.unlock();
					pred = curr;
					curr = (LockListNode<T>) curr.getNext();
					curr.lock();
				}
		
				// Remove node if equals item
				if (curr.compareTo(temp) == 0) {
					pred.setNext(curr.getNext());
				}
			} finally {
				// Unlock curr
				curr.unlock();
			}
		} finally {
			// Unlock pred
			pred.unlock();		
		}
	}

	/**
	 * Print the list without the dummy sentinels.
	 * <T> should also implement the toString() method.
	 */
	@Override
	public String toString() { 
		ListNode<T> curr;
		String output;
		
		curr = _head.getNext();
		output = "[";
			
		while (curr.getNext() != null) {
			output += curr.getItem();
			curr = curr.getNext();
			
			if (curr.getNext() != null) {
				output += ",";
			}
		}
		
		output += "]";	
		
		return output;
	}
}
