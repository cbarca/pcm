package pcm.lists;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import pcm.*;

/**
 * Class CoarseGrainedList
 * 
 * @author Cristian Barca - cba390	
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class CoarseGrainedList<T extends Comparable<T>> implements Sorted<T> {
	// _Private members
	private ListNode<T> _head = null, _tail = null;
	private Lock _lock = new ReentrantLock(); 
	
	public CoarseGrainedList() {
		// Create two sentinels
		_head = new ListNode<T>();
		_head.setAsMinusInfinty();
		
		_tail = new ListNode<T>();
		_tail.setAsPlusInfinty();
		
		_head.setNext(_tail);
	}
	
	/**
	 * Add an item as a node in the list [up-to-front addition].
	 * Coarse blocking method.
	 * @param item = value to insert as a node
	 */
	@Override
	public void add(T item) {
		ListNode<T> pred = null, curr = null, 
				temp = new ListNode<T>(item);
		
		// Lock
		_lock.lock();
		
		try { // Enter critical section
			pred = _head;
			curr = pred.getNext();
			
			// Up-to-front find
			while (curr.compareTo(temp) < 0) {
				pred = curr;
				curr = curr.getNext();
			}
		
			// Add item as a list node
			ListNode<T> node = new ListNode<T>(item);
			node.setNext(curr);
			pred.setNext(node);
		} finally {
			// Unlock
			// Leave critical section
			_lock.unlock();
		}
	}
	
	/**
	 * Remove the first node from the list that equals item.
	 * Coarse blocking method.
	 * @param item = value to delete from the list
	 */
	@Override
	public void remove(T item) {
		ListNode<T> pred = null, curr = null, 
				temp = new ListNode<T>(item);
		
		// Lock		
		_lock.lock();
		
		try { // Enter critical section
			pred = _head;
			curr = pred.getNext();
			
			// Up-to-front find
			while (curr.compareTo(temp) < 0) {
				pred = curr;
				curr = curr.getNext();
			}
			
			// Remove node if equals item 
			if (curr.compareTo(temp) == 0) {
				pred.setNext(curr.getNext());
			}
		} finally {
			// Unlock
			// Leave critical section
			_lock.unlock();
		}
	}

	/**
	 * Print the list without the dummy sentinels.
	 * <T> should also implement the toString() method.
	 */
	@Override
	public String toString() { 
		ListNode<T> curr;
		String output;
		
		curr = _head.getNext();
		output = "[";
			
		while (curr.getNext() != null) {
			output += curr.getItem();
			curr = curr.getNext();
			
			if (curr.getNext() != null) {
				output += ",";
			}
		}
			
		output += "]";
		
		return output;
	}
}