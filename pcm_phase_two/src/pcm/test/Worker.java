package pcm.test;

/**
 * Class Worker.
 * Defines the structure of a worker-thread.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class Worker<T extends Comparable<T>> extends Thread {
	// _Private members
	WorkPool<T> _wp = null;

	/**
	 * Ctor.
	 * @param wp = work pool
	 */
	public Worker(WorkPool<T> wp) {
		_wp = wp;
	}

	void doWork(Work<T> wt) {
		wt.compute();
	}
	
	/**
	 * Run (try to grab a task and compute it).
	 */
	public void run() {
		System.out.println("Worker " + this.getName() + " started ...");
		
		while (true) {
			Work<T> wt = _wp.getWork();
			
			if (wt == null) {
				break;
			}
			
			doWork(wt);
			
			if (wt.isRemovable()) {
				// Remove work.
				_wp.putWork(new Work<T>(wt.getDataStructs(), wt.getOppositeOperation(), 
						wt.getElement(), wt.getWorkTime(), !wt.isRemovable()));
			}
		}
		
		System.out.println("Worker " + this.getName() + " finished ...");
	}	
}



