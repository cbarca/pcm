package pcm.test.performance;

import java.util.ArrayList;

import pcm.Sorted;
import pcm.lists.CoarseGrainedList;
import pcm.lists.FineGrainedList;
import pcm.lists.LockFreeList;
import pcm.test.MultithreadedTest;
import pcm.trees.CoarseGrainedTree;
import pcm.trees.FineGrainedTree;
import pcm.trees.LockFreeTree;

/**
 * Class Performance.
 * Multithreaded performance test of the data structures.
 * Driver for testing a lot of possible scenarios using
 * the data structures (adds followed by removes, adds
 * overalpped with removes, etc)
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 * 
 */
public class Performance extends MultithreadedTest {
	private static final String CGL = "cgl";
	private static final String CGT = "cgt";
	private static final String FGL = "fgl";
	private static final String FGT = "fgt";
	private static final String LFL = "lfl";
	private static final String LFT = "lft";

	public Performance(ArrayList<Sorted<Integer>> dstructs, int nThreads,
			int nAdd, int nRAdd, int nRemove, int workTime, boolean allowDup) {
		super(dstructs, nThreads, nAdd, nRAdd, nRemove, workTime, allowDup);
	}

	private static void exitWithError() {
		System.out.println("test data structures " +
				"<dataStructure> <nThreads> <nAdds> <nRAdds> <nRemoves> <workTime> <allowDuplicates>");
		System.out.println("  where:");
		System.out.printf("     <data_structure> in {%s, %s, %s, %s, %s, %s}\n", 
				CGL, CGT, FGL, FGT, LFL, LFT);
		System.out.println("    <nThreads> is a number > 0");
		System.out.println("    <nAdds> is a number > 0");
		System.out.println("    <nRAdds> is a number > 0");
		System.out.println("    <nRemoves> is a number > 0");
		System.out.println("    <workTime> is a number >= 0 (micro seconds)");
		System.out.println("    <allowDuplicates> is a boolean value");
		System.exit(1);
	}

	public static void main(String args[]) {
		if (args.length != 7) {
			exitWithError();
		}

		String dstruct = args[0];

		int nThread = Integer.parseInt(args[1]);
		if (nThread < 1) {
			exitWithError();
		}

		int nAdd = Integer.parseInt(args[2]);
		if (nAdd < 0) {
			exitWithError();
		}

		int nRAdd = Integer.parseInt(args[3]);
		if (nRAdd < 0) {
			exitWithError();
		}

		int nRemove = Integer.parseInt(args[4]);
		if (nRemove < 0) {
			exitWithError();
		}

		int workTime = Integer.parseInt(args[5]);
		if (workTime < 0) {
			exitWithError();
		}
		
		boolean allowDup = Boolean.parseBoolean(args[6]);

		System.out.println("dStruct: " + dstruct);
		System.out.println("nThread: " + nThread);
		System.out.println("nAdd: " + nAdd);
		System.out.println("nRAdd: " + nRAdd + " x2");
		System.out.println("nRemove: " + nRemove);
		System.out.println("workTime: " + workTime);
		System.out.println("allowDup: " + allowDup);

		ArrayList<Sorted<Integer>> dstructs = new ArrayList<Sorted<Integer>>();

		if (dstruct.equals(CGL)) {
			dstructs.add(new CoarseGrainedList<Integer>());
		} 
		else if (dstruct.equals(CGT)) {
			dstructs.add(new CoarseGrainedTree<Integer>());
		} 
		else if (dstruct.equals(FGL)) {
			dstructs.add(new FineGrainedList<Integer>());
		} 
		else if (dstruct.equals(FGT)) {
			dstructs.add(new FineGrainedTree<Integer>());
		} 
		else if (dstruct.equals(LFL)) {
			dstructs.add(new LockFreeList<Integer>());
		} 
		else if (dstruct.equals(LFT)) {
			dstructs.add(new LockFreeTree<Integer>());
		} 
		else {
			exitWithError();
		}

		// Multithreaded performance test (only for <T> as <Integer>)
		System.out.println("\nMultithreaded performance test");

		Performance performanceTest = new Performance(
				dstructs, nThread, 
				nAdd, nRAdd, nRemove, 
				workTime, allowDup);		

		long tstart = System.currentTimeMillis();
		performanceTest.run();
		long tend = System.currentTimeMillis();

		System.out.println("\ntime: " + (tend - tstart) + " ms"); 
	}
}
