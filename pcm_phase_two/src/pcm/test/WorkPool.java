package pcm.test;

import java.util.LinkedList;

/**
 * Class WorkPool
 * Implements a "work pool" based on "replicated workers" paradigm.
 * All the tasks that are inserted in the work pool are objects of Work class.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 * 
 * @param <T> extends Comparable<T> 
 */
public class WorkPool<T extends Comparable<T>> {
	// _Private members
	private int _nThreads; 
	private int _nWaiting = 0; 
	private boolean _getReady = false, // Workers are done with the pool.
			_putReady = false; // No more adding work tasks for the threads. 
	LinkedList<Work<T>> _tasks = null;

	/**
	 * Ctor.
	 * @param nThreads = number of worker threads
	 */
	public WorkPool(int nThreads) {
		_tasks = new LinkedList<Work<T>>();
		_nThreads = nThreads;
	}

	/**
	 * Try to get a work-task from the work pool.
	 * If there are no available tasks we wait until somebody can 
	 * give us some work to do or until the problem is finished/completed.
	 * @return Work object / null if the problem is solved
	 */
	public synchronized Work<T> getWork() {
		if (_tasks.size() == 0) { 
			_nWaiting++;
			
			if (_nWaiting == _nThreads && _putReady) {
				// Work pool is empty, no more puts and 
				// all the the threads are waiting.
				_getReady = true; // Program is finished.
				
				notifyAll();
				
				return null;
			} 
			else {
				while (!_getReady && _tasks.size() == 0) {
					try {
						this.wait();
					} 
					catch(Exception e) {
						e.printStackTrace();
					}
				}
				
				if (_getReady) {
					// Program finished.
				    return null;
				}

				_nWaiting--;	
			}
		}
		
		return _tasks.remove();
	}

	/**
	 * Get the size of the work pool.
	 * @return the size of the wp
	 */
	public int getSize() {
		return _tasks.size();
	}
	
	/**
	 * Put the last work task in the work pool.
	 * @param wt = work task to be put in the work pool 
	 */
	public synchronized void putLastWork(Work<T> wt) {
		_putReady = true;
		putWork(wt);
	}
	
	/**
	 * Put a work task in the work pool.
	 * @param wt = work task to be put in the work pool 
	 */
	public synchronized void putWork(Work<T> wt) {
		_tasks.add(wt);
		this.notifyAll();
	}
}


