package pcm.test.validation;

import java.util.ArrayList;

import pcm.Sorted;
import pcm.lists.CoarseGrainedList;
import pcm.lists.FineGrainedList;
import pcm.lists.LockFreeList;
import pcm.test.MultithreadedTest;
import pcm.trees.CoarseGrainedTree;
import pcm.trees.FineGrainedTree;
import pcm.trees.LockFreeTree;

/**
 * Class Validation.
 * Multithreaded validation test of the data structures.
 * Tests the correctness of the data structures by executing
 * Adds combined with RAdds operations (normal adds + removable
 * adds).
 * In the end a toString() call should look the same for all 
 * of the data structures (this is why we let only Adds and
 * RAdds operations, we could control RAdds but Removes would have
 * eliminate random elements - unpredictable). 
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 */
public class Validation extends MultithreadedTest {
	
	public Validation(ArrayList<Sorted<Integer>> dstructs, int nThreads, 
			int nAdd, int nRAdd, boolean allowDup) {
		super(dstructs, nThreads, nAdd, nRAdd, 0, 1, allowDup);
	}
	
	private static void exitWithError() {
		System.out.println("test all data structures " +
				"<nThreads> <nAdds> <nRAdds> <allowDuplicates");
		System.out.println("  where:");
		System.out.println("    <nThreads> is a number > 0");
		System.out.println("    <nAdds> is a number >= 0");
		System.out.println("    <nRadds> is a number > 0");
		System.out.println("    <allowDuplicates> is a boolean value");
		System.exit(1);
	}

	public static void main(String args[]) {
		if (args.length != 4) {
			exitWithError();
		}

		int nThread = Integer.parseInt(args[0]);
		if (nThread < 1) {
			exitWithError();
		}

		int nAdd = Integer.parseInt(args[1]);
		if (nAdd < 0) {
			exitWithError();
		}
		
		int nRAdd = Integer.parseInt(args[2]);
		if (nRAdd < 1) {
			exitWithError();
		}
		
		boolean allowDup = Boolean.parseBoolean(args[3]);

		System.out.println("nThread: " + nThread);
		System.out.println("nAdd: " + nAdd);
		System.out.println("nRAdd: " + nRAdd + " x2");
		System.out.println("allowDup: " + allowDup);
		
		// The beauty of using interfaces 
		Sorted<Integer> cgl = new CoarseGrainedList<Integer>();
		Sorted<Integer> fgl = new FineGrainedList<Integer>();
		Sorted<Integer> lfl = new LockFreeList<Integer>();
		Sorted<Integer> cgt = new CoarseGrainedTree<Integer>();
		Sorted<Integer> fgt = new FineGrainedTree<Integer>();
		Sorted<Integer> lft = new LockFreeTree<Integer>();
		
		ArrayList<Sorted<Integer>> dstructs = new ArrayList<Sorted<Integer>>();
		dstructs.add(cgl);
		dstructs.add(fgl);
		dstructs.add(lfl);
		dstructs.add(cgt);
		dstructs.add(fgt);
		dstructs.add(lft);
		
		// Multithreaded validation test (only for <T> as <Integer>)
		System.out.println("\nMultithreaded validation test");

		Validation validationTest = new Validation(dstructs, nThread, nAdd, nRAdd, allowDup);		

		long tstart = System.currentTimeMillis();
		validationTest.run();
		long tend = System.currentTimeMillis();
				
		System.out.println("\nResults after applying the test: [the lists should contain" +
				"nAdd elements, except the LFT which doesn't allow duplicates]");
		for (Sorted<Integer> ds : dstructs) {
			System.out.println(ds);
		}
						
		System.out.println("\ntime: " + (tend - tstart) + " ms");
	}
}
