package pcm.test;

import java.util.ArrayList;

import pcm.Sorted;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Class Work.
 * Defines the work task for a thread.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 *
 * @param <T> extends Comparable<T>
 */
public class Work<T extends Comparable<T>> {
	public static enum Operation {
		ADD, REMOVE;
	}
	
	// _Private members
	private Operation _op = null;
	private T _element = null;
	private ArrayList<Sorted<T>> _dstructs = null;
	private int _workTime;
	private boolean _removableWork;
	
	/**
	 * Ctor.
	 * @param dstruct = data structures to operate on
	 * @param op = operation to execute/compute
	 * @param element = operation's element
	 * @param workTime = time to spend on work
	 * @param removableWork = mark the work as removable operation
	 */
	public Work(ArrayList<Sorted<T>> dstructs, Operation op, T element, 
			int workTime, boolean removableWork) {
		_dstructs = dstructs;
		_op = op;
		_element = element;
		_workTime = workTime;
		_removableWork = removableWork;
	}
	
	public boolean isRemovable() {
		return (_op.equals(Operation.ADD) && _removableWork);
	}
	
	public Operation getOppositeOperation() {
		return _op.equals(Operation.ADD) ? Operation.REMOVE : Operation.ADD; 
	}
	
	public Operation getOperation() {
		return _op;
	}
	
	public ArrayList<Sorted<T>> getDataStructs() {
		return _dstructs;
	}
	
	public T getElement() {
		return _element;
	}
	
	public int getWorkTime() {
		return _workTime;
	}
	
	/**
	 * Compute work.
	 */
	public void compute() {
		long end = System.nanoTime() + _workTime * 1000;
		while (System.nanoTime() < end); // Busy wait.
		
		switch (_op) {
		case ADD:
			for (Sorted<T> ds : _dstructs) {
				ds.add(_element);
			}
			break;
		case REMOVE:
			for (Sorted<T> ds : _dstructs) {
				ds.remove(_element);
			}
			break;
		default:
			throw new NotImplementedException();
		}
	}
}

