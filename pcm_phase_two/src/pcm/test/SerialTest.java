package pcm.test;

import pcm.Sorted;
import pcm.lists.CoarseGrainedList;
import pcm.lists.FineGrainedList;
import pcm.lists.LockFreeList;
import pcm.trees.CoarseGrainedTree;
import pcm.trees.FineGrainedTree;
import pcm.trees.LockFreeTree;

/**
 * Class SerialTest.
 * Applies a serial test over the all
 * data structures.
 * 
 * @author Cristian Barca - cba390
 * @author Liviu Razorea - lra230
 */
public class SerialTest {
	public static final int N_LIST = 50;
	
	public static void simpleSerialTest(Sorted<Integer> ds, String typeTest) {
		System.out.println(typeTest);
		
		// Add N_LIST elements
		for (int i = 1; i < N_LIST; i++) {
			ds.add(new Integer(i));
		}
		
		// Print
		System.out.println(ds);
		
		// Remove 1..N_LIST/2
		for (int i = 1; i < N_LIST / 2; i++) {
			ds.remove(new Integer(i));
		}
		
		// Print
		System.out.println(ds.toString());
		
		// Remove N_LIST/2..N_LIST odd numbers
		for (int i = N_LIST / 2; i < N_LIST; i += 2) {
			ds.remove(new Integer(i));
		}
		
		// Separate adds
		ds.add(new Integer(37));
		ds.add(new Integer(48));
		ds.add(new Integer(25));
		ds.add(new Integer(44));
		
		// Print
		System.out.println(ds);
		
		// Remove N_LIST/2..N_LIST even numbers [once]
		for (int i = N_LIST / 2 - 1; i < N_LIST; i += 2) {
			ds.remove(new Integer(i));
		}
		
		// Print
		System.out.println(ds);
		
		// Separate removes
		ds.remove(new Integer(37));
		ds.remove(new Integer(48));
		ds.remove(new Integer(25));
		ds.remove(new Integer(44));
		
		// Print
		System.out.println(ds);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// The beauty of using interfaces 
		Sorted<Integer> cgl = new CoarseGrainedList<Integer>();
		Sorted<Integer> fgl = new FineGrainedList<Integer>();
		Sorted<Integer> lfl = new LockFreeList<Integer>();
		Sorted<Integer> cgt = new CoarseGrainedTree<Integer>();
		Sorted<Integer> fgt = new FineGrainedTree<Integer>();
		Sorted<Integer> lft = new LockFreeTree<Integer>();
		
		// Serial Validation Tests
		SerialTest.simpleSerialTest(cgl, "CoarseGrainedList serial validation test");
		SerialTest.simpleSerialTest(fgl, "FineGrainedList serial validation test");
		SerialTest.simpleSerialTest(lfl, "LockFreeList serial validation test");
		SerialTest.simpleSerialTest(cgt, "CoarseGrainedTree serial validation test");
		SerialTest.simpleSerialTest(fgt, "FineGrainedTree serial validation test");
		SerialTest.simpleSerialTest(lft, "LockFreeTree serial validation test - NO DUPLICATES ALOWED");
	}
}
